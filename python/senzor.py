import bme680
import time
import datetime
import mysql.connector

sensor = bme680.BME680()

sensor.set_humidity_oversample(bme680.OS_2X)
sensor.set_pressure_oversample(bme680.OS_4X)
sensor.set_temperature_oversample(bme680.OS_8X)
sensor.set_filter(bme680.FILTER_SIZE_3)

sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
sensor.set_gas_heater_temperature(320)
sensor.set_gas_heater_duration(150)
sensor.select_gas_heater_profile(0)

mydb = mysql.connector.connect(
 host="localhost",
 user="root",
 passwd="misopoit",
 database="mysql"
)

mycursor = mydb.cursor()

x = 0

while True:
	print(datetime.datetime.now())

	if sensor.get_sensor_data():
		
		if sensor.data.heat_stable:
			sql = "INSERT INTO meteo (temper, press, hum, res, datum) VALUES (%s, %s, %s, %s, %s)"
			trz = datetime.datetime.now()
			val = (sensor.data.temperature, sensor.data.pressure, sensor.data.humidity, sensor.data.gas_resistance, trz)
			mycursor.execute(sql, val)
			mydb.commit()
			break
			
		if x == 5:
			sql = "INSERT INTO meteo (temper, press, hum, datum) VALUES (%s, %s, %s, %s)"
			trz = datetime.datetime.now()
			val = (sensor.data.temperature, sensor.data.pressure, sensor.data.humidity, trz)
			mycursor.execute(sql, val)
			mydb.commit()
			break
			
		else:
			x += 1
			
	time.sleep(10)
		
