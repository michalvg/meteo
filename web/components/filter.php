          <div class="row">
            <div class="col-lg-12">


<?php
                      $x = 0;
                      $strT = "[";
                      $strD = "[";
                      $strP = "[";
                      $strH = "[";
                      $strR = "[";
                    
                      $sql = "SELECT * FROM meteo";
                      $results = $conn->query($sql);
                      if($results->num_rows > 0){
                        while($row = $results->fetch_assoc()){
                          
                          $temper = $row["temper"];
                          $date = $row["datum"];
                          $press = $row["press"];
                          $hum = $row["hum"];
                          $res = $row["res"];
                          
                          if($x == 0){ 
                            $strT .= $temper;
                            $strP .= $press;
                            $strH .= $hum;
                            $strR .= $res;
                            $strD .= "'" . $date . "'";
                            $x = 1;
                          }
                          else {
                            $strT .= ", " . $temper;
                            $strP .= ", " . $press;
                            $strH .= ", " . $hum;
                            $strR .= ", " . $res;
                            $strD .= ", '" . $date . "'";
                          }                          
                          
                        }
                      }
                      $strT .= "]";
                      $strD .= "]";
                      $strP .= "]";
                      $strH .= "]";
                      $strR .= "]";
                          
              ?>
              
              <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Now</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <canvas id="canvasTemp"></canvas><canvas id="canvasHum"></canvas>
                </div>
              </div>
              <!-- end -->
              
              <script>
                var gauge = new RadialGauge({
          renderTo: 'canvasTemp',
          width: 300,
          height: 300,
          units: "Temperature C",
          minValue: -45,
          maxValue: 45,
          majorTicks: [
              "-45",
              "-40",
              "-35",
              "-30",
              "-25",
              "-20",
              "-15",
              "-10",
              "-5",
              "0",
              "5",
              "10",
              "15",
              "20",
              "25",
              "30",
              "35",
              "40",
              "45"
          ],
          minorTicks: 5,
          strokeTicks: true,
          highlights: [
              {
                  "from": 25,
                  "to": 45,
                  "color": "rgba(200, 50, 50, .75)"
              }
          ],
          colorPlate: "#fff",
          borderShadowWidth: 0,
          borders: false,
          needleType: "arrow",
          needleWidth: 2,
          needleCircleSize: 7,
          needleCircleOuter: true,
          needleCircleInner: false,
          animationDuration: 1500,
          animationRule: "linear"
      });
      gauge.draw();
      gauge.value = <?php echo $temper ?>;
      
      
      var gauge2 = new RadialGauge({
          renderTo: 'canvasHum',
          width: 300,
          height: 300,
          units: "Humidity %",
          minValue: 0,
          maxValue: 100,
          majorTicks: [
              "0",
              "5",
              "10",
              "15",
              "20",
              "25",
              "30",
              "35",
              "40",
              "45",
              "50",
              "55",
              "60",
              "65",
              "70",
              "75",
              "80",
              "85",
              "90",
              "95",
              "100"
          ],
          minorTicks: 5,
          strokeTicks: true,
          highlights: [
              {
                  "from": 75,
                  "to": 100,
                  "color": "rgba(200, 50, 50, .75)"
              }
          ],
          colorPlate: "#fff",
          borderShadowWidth: 0,
          borders: false,
          needleType: "arrow",
          needleWidth: 2,
          needleCircleSize: 7,
          needleCircleOuter: true,
          needleCircleInner: false,
          animationDuration: 1500,
          animationRule: "linear"
      });
      gauge2.draw();
      gauge2.value = <?php echo $hum ?>;
              </script>
              
              
              

              <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Temperature</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div id='temp'></div>
                </div>
              </div>
              <!-- end -->
              
              <script>
                var data = [
                    {
                      x: <?php echo $strD ?>,
                      y: <?php echo $strT ?>,
                      type: 'scater'
                    }
                  ];

              Plotly.newPlot('temp', data);
              </script>
              
              
              <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Pressuere</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div id='press'></div>
                </div>
              </div>
              <!-- end -->
              
              <script>
                var data = [
                    {
                      x: <?php echo $strD ?>,
                      y: <?php echo $strP ?>,
                      type: 'scater'
                    }
                  ];

              Plotly.newPlot('press', data);
              </script>
              
              <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Humidity</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div id='hum'></div>
                </div>
              </div>
              <!-- end -->
              
              <script>
                var data = [
                    {
                      x: <?php echo $strD ?>,
                      y: <?php echo $strH ?>,
                      type: 'scater'
                    }
                  ];

              Plotly.newPlot('hum', data);
              </script>
              
              
              <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Air condition</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div id='res'></div>
                </div>
              </div>
              <!-- end -->
              
              <script>
                var data = [
                    {
                      x: <?php echo $strD ?>,
                      y: <?php echo $strR ?>,
                      type: 'scater'
                    }
                  ];

              Plotly.newPlot('res', data);
              </script>

              <!-- Collapsable Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                  <h6 class="m-0 font-weight-bold text-primary">Vcera</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardExample">
                  <div class="card-body">
                    Namerane hodnoty <strong>DB hodnoty</strong> 
                  </div>
                </div>
              </div>

            </div>
          </div>
