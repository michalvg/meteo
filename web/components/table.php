
          <!-- DataTale -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Table</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Temperature</th>
                      <th>Pressure</th>
                      <th>Humidity</th>
                      <th>Date</th>
                     
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Temperature</th>
                      <th>Pressure</th>
                      <th>Humidity</th>
                      <th>Date</th>
                      
                    </tr>
                  </tfoot>
                  <tbody>
                    
                    <?php
                    
                      $sql = "SELECT * FROM meteo";
                      $results = $conn->query($sql);
                      if($results->num_rows > 0){
                        while($row = $results->fetch_assoc()){
                          $temper = $row["temper"];
                          $press = $row["press"];
                          $hum = $row["hum"];
                          $datum = $row["datum"];
                          
                          ?>
                          <tr>
                            <td><?php echo $temper . " C"?></td>
                            <td><?php echo $press . " hPa"?></td>
                            <td><?php echo $hum . " %"?></td>
                            <td><?php echo $datum ?></td>
                            
                          </tr>
                          
                          <?php
                          
                        }
                      }
                      
                    
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        

  

  


