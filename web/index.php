<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>POIT</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script src="lib/plotly.js"></script>
  <script src="lib/gauge.min.js"></script>

</head>

<?php 
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    require_once("scripts/connect.php");
    session_start();
    
    ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!--  Sidebar -->
    <?php
		
		require_once("components/sidebar.php");
		
    ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php
			
			require_once("components/topbar.php");
			
        ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          
          
          <!-- Filter -->
          <?php if(!isset($_GET["table"])){
			
          ?>
          <h1 class="h3 mb-4 text-gray-800">Graphs</h1>
          
          <?php
               	}
                
                	else {
          ?>
          
             <h1 class="h3 mb-2 text-gray-800">Historia zaznamov</h1>
             
             <?php
                    }
             ?>
             
             <?php
                  if(isset($_GET["table"]) && ($_GET["table"] == "active")){
                    require_once("components/table.php");
                  }
                  else require_once("components/filter.php");
			
          ?>
          <!-- End of Filter -->

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
        <?php
			
			require_once("components/footer.php");
			
        ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Extensions -->
  <?php
  
	require_once("components/extensions.php");
	
  ?>
  <!-- End of Extensions -->
  

</body>

</html>
